.PHONY=build-all-docker-images build-jdk17-latest build-jdk16-latest build-jdk8-latest remove-none-docker-images clean

build-all-docker-images: | build-jdk17-latest build-jdk17-latest build-jdk8-latest

build-jdk17-latest:
	docker build -t jdk17 ./images/ -f ./images/Dockerfile_17

build-jdk16-latest:
	docker build -t jdk16 ./images/ -f ./images/Dockerfile_16

build-jdk8-latest:
	docker build -t jdk8 ./images/ -f ./images/Dockerfile_8

container-prune:
	docker container prune

remove-none-docker-images:
	docker rmi $(shell docker images --filter "dangling=true" -q)

clean: | container-prune remove-none-docker-images
