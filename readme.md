# Entering a Container

docker attach <container-name>

# Leaving a Container

A Key Sequence of:
`Ctrl+P + Ctrl+Q`

# Run a Container
docker run --name sshd_app -t -i -p 2222:22 -p 25565:25565 --mount type=bind,source="$(pwd)"/vanilla,target=/mounted alpine-ssh:latest

Options:
--restart always: 

# Shell in Container
docker exec -it sshd_app /bin/sh


# Setting-Up a server

1. Setup the right Docker image with the right JDK version.
2. Create a folder int the `packs` folder and paste the correct `server.jar` as well as a copied `entrypoint.sh` file into this folder.
3. Create a new service in the `docker-compose.yaml` file. Adjust the settings you wanna have, like matching folder path within the packs folder,
  port and image.
4. Execute `docker compose up <service name>` once until it restarts.
5. Accept the Eula in `packs/<your-minecraft-server-folder/eula.txt>` by replacing `false` with `true`.
6. Adjust your Server-Configs in the `server.properties` file. How to [here](https://minecraft.fandom.com/de/wiki/Server.properties).
7. Re-run `docker compose up <service name>`
8. Add `entrypoint.sh` and `<server.jar>` to git with `add <path>/entrypoint.sh -f` and `add <path>/server.jar -f` since it is in the `.gitignore` otherwise.
9. Happy Playing.

PS: At bukkit, you can also download `<pluginname>.jar` files(`wget`) and put it into the created `plugins` folder.

