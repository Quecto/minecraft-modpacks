#!/bin/sh

exec /usr/sbin/sshd -D -e "$@" &
exec java -Xmx1024M -Xms1024M -jar server.jar nogui
